#!/usr/bin/bash

#The user profile
username="nassimgc"
password="azerty1234"
first_name="Nassim"
last_name="BOUGTIB"
age=21
email="nassim.bougtib@hotmail.com"

#We save the input for the username field when the user want to login
username_input=$1
#We save the input for the password field when the user want to login
password_input=$2

#The login function. The user need to give the username and the password
login()
{
    if [ "$username_input" != "$username" ] || [ "$password_input" != "$password" ]; then

        until [ "$username_input" = "$username" ] && [ "$password_input" = "$password" ]; do

            #The user type his username
            echo "Please enter your username"
            read username_input

            #The username type his password
            echo "Please enter your password"
            read -s password_input

            #If the user give the wrong username and password it would show an error message.
            if [ "$username_input" != "$username" ] || [ "$password_input" != "$password" ]; then
                echo "Wrong login !"
            fi


        done
    fi
}


#Give the version of the prompt
versionOfBash()
{
    echo "The version of my magic prompt is 1.0.0"
}


#Function that will answer if the user is minor or an adult
ageOfUser()
{
    #We ask the user to type an age
    echo "Please enter your age $username"
    read age_user

    #We determine if the user have 18 yo or more : is an adult
    if [ $age_user -ge 18 ]; then
        echo "${username} you are an adult !"
    else
        #if the user have less then 18 yo : is a minor
        echo "${username} you are a minor !"
    fi
}


#Function that will display the profile of the user with the cmd "profil"
profileOfUser()
{
    echo -e "Your first name : $first_name"
    echo -e "Your last name : $last_name"
    echo -e "Your age : ${age}"
    echo -e "Your email : $email"
}

#Function that will display the current hour with the cmd "hour"
displayHour()
{
    echo "$(date +%H) hour"
}

#Function which download a source code of a web site with the cmd "httpget"
downloadPage()
{
    #We check if the user give an argument
    if [ -z "$1" ]; then
        echo "You need to give a website"
    else
        #If the user give the argument he choose the name of the file of his download
        echo "How do you want to name your file ?"
        read file_name
        curl $1 > $file_name
    fi
}



#Function that will display all command of the prompt with the cmd "help"

helpCommand()
{
    echo "ls : will give the list of all files and directories (even hidden) of the current path. You can optionnally give a path in parameter.
     rm path_file : 
        Will remove a file. You need to give the path of the file.

    rmd path_directory | rmdir path_directory : 
        Will remove a directory. You need to give the path of the directory.

    about : 
        Give the description of the magic prompt.

    version | --version | -v : 
        Will give the version of the magic prompt.

    age age_number : 
        It will determine if you are an adult or a minor.

    quit | exit : 
        This command stop the prompt.

    profile : 
        Will give your personal information
 
    passw : 
        Allow you to change your password.
 
    cd path : 
        Redirect you to a path.

    pwd : 
        Will give you the current path.

    hour : 
        Will give the current hour.

    httpget website : 
        Will download the source code of a website you will send in parameter.

    smtp : 
        Will send an email.

    open path_file : 
        Will open in vim a file."
}

#Function the description of the prompt with cmd "about"
aboutCommand()
{
    echo "This magic prompt have a lot of features like download the source code of web page, create file, create directory... "
    echo "To know all the commands, you can use help command "
}

changePasswordUser(){


    #Ask if the user want really to change the password
    until [ "$answer_change_password" = 'Y' ] || [ "$answer_change_password" = 'y' ] || [ "$answer_change_password" = 'n' ] || [ "$answer_change_password" = 'N' ]; do

            echo "Do you want change your password ? [Y/N]"
            read answer_change_password

    done
    #If the user select Y : 
    if [ $answer_change_password = 'Y' ]  || [ $answer_change_password = 'y' ]; then
            first_password='a'
            confirm_password='b'
            #Until 1st type of the password is the same as the confirmation
            until [ "$first_password" = "$confirm_password" ]; do
               
                echo -e "Please write your new password."
                read -s first_password

                echo -e "Please confirm your new password."
                read -s confirm_password
                #Check if the first type of the password is different of the confirmation
                if [ "$first_password" != "$confirm_password" ]; then

                    echo -e "The password are not the same, please try again."

                fi

            done
            #The new password is now available
            password=$first_password
            
    fi

    answer_change_password=" "
}


#Function that will open a vim with a name as an argument with cmd "open"
openCommand()
{
    #We check if the user give an argument
    if [ -z "$1" ]; then
        echo -e "You need to give a name file or path of file"
    else
        #Will  open a vim of the file give in argument
        vim $1
    fi
}


redirectCommand()
{
    #We check if the user give an argument
    if [ -z "$1" ]; then
        echo -e "You need to give a path"
    else
        #Will redirect to the path give in argument
        cd $1
    fi
}

removeDirCommand()
{
    #We check if the user give an argument
    if [ -z "$1" ]; then
        echo -e "You need to give a path."
    else
        #Will delete the directory give in argument
        rmdir $1
    fi    
}
cmd()
{

    #Launch login function to log to the prompt before use
    login
    while [[ true ]]; do
        echo -e "Welcome to the magic prompt."
        echo -e "Type a command or use help to know all the commands of the prompt."
        #echo -e $password
        read cmd arg1
        case "$cmd" in
            pwd ) pwd;;
            help ) helpCommand;;
            ls ) ls -la $arg1;;
            version|--version|--v) versionOfBash;;
            age ) ageOfUser;;
            profile) profileOfUser;;
            passw) changePasswordUser;;
            rm ) rm $arg1;;
            rmd | rmdir ) removeDirCommand $arg1;;
            about ) aboutCommand;;
            cd ) redirectCommand $arg1;;
            hour ) displayHour;;
            open ) openCommand $arg1;;
            httpget ) downloadPage $arg1;;
            quit | exit ) exit 16252;;
            * ) echo "unknown command";;
        esac
    done
}

main(){

    cmd
    
}

main