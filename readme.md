# My Magic Prompt

## Launch the prompt

To launch the prompt you need to be in the folder of the project and launch the command : `./main.sh username password`

You need to determine the username and the password at the launch but if you type a wrong username and password, you can try again.

If you don't want to connect you need to use : `Ctrl+c`

## Features

When you are loggin, you can use the prompt.

There is the list of the commands that you can use in the magic prompt. 

Just launch the `help` command and you will see the following commands :


* ls:  will give the list of all files and directories (even hidden) of the current path. You can optionnally give a path in parameter.

* rm **path_file** : Will remove a file. You need to give the path of the file.

* rmd **path_directory** | rmdir **path_directory** :  Will remove a directory. You need to give the path of the directory.

* about :  Give the description of the magic prompt.

* version | --version | -v :  Will give the version of the magic prompt.

* age **age_number** :  It will determine if you are an adult or a minor.

* quit | exit :  This command stop the prompt.

* profile : Will give your personal information

* passw : Allow you to change your password.

* cd **path** :  Redirect you to a path.

* pwd :  Will give you the current path.

* hour :  Will give the current hour.

* httpget **website** :  Will download the source code of a website you will send in parameter.

* smtp :  Will send an email.

* open **path_file** : Will open in vim a file."
